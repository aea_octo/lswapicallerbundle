<?php
namespace Lsw\ApiCallerBundle\Call;

use Lsw\ApiCallerBundle\Helper\Curl;

/**
 * cURL based API call with request data send as GET parameters
 *
 * @author Maurits van der Schee <m.vanderschee@leaseweb.com>
 */
class HttpPutJson extends CurlCall implements ApiCallInterface
{
    private $responseHeaders;

    /**
     * {@inheritdoc}
     */
    public function generateRequestData()
    {
        $this->requestData = json_encode($this->requestObject);
    }

    /**
     * {@inheritdoc}
     */
    public function parseResponseData()
    {
        $this->responseObject = json_decode($this->responseData,$this->asAssociativeArray);
    }

    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * {@inheritdoc}
     */
    public function makeRequest($curl, $options)
    {
        $curl->setopt(CURLOPT_URL, $this->url);
        $curl->setopt(CURLOPT_POST, 1);
        $curl->setopt(CURLOPT_POSTFIELDS, $this->requestData);
        $curl->setopt(CURLOPT_HEADER, true);
        $curl->setopt(CURLOPT_CUSTOMREQUEST, "PUT");
        $curl->setopt(
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->requestData)
            ]
        );
        $curl->setoptArray($options);
        $content = $curl->exec();
        $header_size = $curl->getinfo(CURLINFO_HEADER_SIZE);
        $header = substr($content, 0, $header_size);
        $this->responseData = substr($content, $header_size);
        $this->responseHeaders = $this->parseHeaders($header);
    }

    private function parseHeaders($headerContent)
    {
        $headers = [];

        $arrRequests = explode("\r\n\r\n", $headerContent);

        foreach ($arrRequests as $arrRequest) {
            $header = [];
            foreach (explode("\r\n", $arrRequest) as $i => $line) {
                if ($i === 0) {
                    $header['http_code'] = $line;
                } else {
                    list ($key, $value) = explode(': ', $line);
                    $header[$key] = $value;
                }
            }
            $headers[] = $header;
        }

        return $headers;
    }
}
